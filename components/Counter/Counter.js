import React, { useState } from "react";
import styles from "./Counter.scss";
console.log(styles);

function Counter() {
  const [count, setCount] = useState(2);

  const decrementCount = () => setCount(count - 1);
  const incrementCount = () => setCount(count + 1);

  const totalPrice = count * 100;

  const changeValue = e => {
    if (e.target.value <= 15 && e.target.value >= 2) {
      setCount(e.target.value);
    }
  };

  return (
    <div className={styles.counter}>
      <h1 className={styles.counter__title} data-testid="counterTitle">
        ¿En cuántas oficinas quieres usar este producto?
      </h1>

      <div className={styles.counter__actions}>
        <button
          className={`${styles.counter__button} ${styles.subtract}`}
          data-testid="decrementButton"
          type="button"
          onClick={decrementCount}
        />
        <input
          data-testid="input"
          type="number"
          value={count}
          min="2"
          max="15"
          onChange={changeValue}
        />
        <button
          className={`${styles.counter__button} ${styles.add}`}
          data-testid="incrementButton"
          type="button"
          onClick={incrementCount}
        />
      </div>

      <div className={styles.counter__text}>
        <p>
          <span data-testid="officeNumber">{count} </span>oficinas con XPrivacy{" "}
        </p>
        <p>
          El precio calculado es{" "}
          <span data-testid="totalPrice">{totalPrice}€/mes</span>
        </p>
      </div>
    </div>
  );
}

export default Counter;
