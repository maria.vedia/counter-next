import React from "react";
import { render, fireEvent, getByTestId } from "react-testing-library";
import Counter from "./Counter";

describe("Counter", () => {
  it("should render Counter without errors", () => {
    const { container } = render(<Counter />);
    expect(container.outerHTML).toBeDefined();
  });

  it("should render the Counter title ", () => {
    const { container } = render(<Counter />);
    const title = container.querySelector("h1");
    expect(title).toHaveTextContent(
      "¿En cuántas oficinas quieres usar este producto?"
    );
  });

  it("should render an input with type number", () => {
    const { container } = render(<Counter />);
    const input = container.querySelector("input");
    expect(input).toHaveAttribute("type", "number");
  });

  it("should render an input with min attribute equal 2 and max attribute equal 15", () => {
    const { container } = render(<Counter />);
    const input = container.querySelector("input");
    expect(input).toHaveAttribute("min", "2");
    expect(input).toHaveAttribute("max", "15");
  });

  it("should render an input with type number", () => {
    const { container } = render(<Counter />);
    const input = container.querySelector("input");
    expect(input).toHaveAttribute("value", "2");
  });

  it("should render a button with not class disabled", () => {
    const { container } = render(<Counter />);
    const button = container.querySelector("button");
    expect(button).not.toHaveAttribute("class", "disabled");
  });

  it("should check if initial input value is 2", () => {
    const { container } = render(<Counter />);
    const initialValue = container.querySelector("input").value;
    expect(initialValue).toBe("2");
  });
});

//Con rerun
//https://spectrum.chat/react-testing-library/general/testing-a-controlled-input~213b5881-77bd-4ea2-93c4-63283c49cc87

//Sin rerun
//https://codesandbox.io/s/n3rvy891n4
