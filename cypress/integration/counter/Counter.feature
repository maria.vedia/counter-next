Feature: Counter

I want to open my Counter App

Scenario: Opening my Counter App
  Given I open my Counter App
  Then I see "¿En cuántas oficinas quieres usar este producto" in the title

Scenario: Checking if increment button works
  Given I check increment button works
  When I double click on the increment button
  Then I see that input value is "4"

Scenario: Checking if decrement button works
  Given I check decrement button works
  When I double click on the decrement button
  Then I see that input value is "0"