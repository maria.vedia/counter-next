import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const url = "http://localhost:3000/";

Given(/^I check decrement button works$/, function() {
  cy.visit(url);
});

When(/^I double click on the decrement button$/, function() {
  cy.getByTestId("decrementButton").click();
  cy.getByTestId("decrementButton").click();
});

Then(/^I see that input value is "0"$/, function() {
  cy.getByTestId("input").should("have.attr", "value", "0");
});
