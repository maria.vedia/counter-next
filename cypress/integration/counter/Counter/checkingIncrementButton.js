import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const url = "http://localhost:3000/";

Given(/^I check increment button works$/, function() {
  cy.visit(url);
});

When(/^I double click on the increment button$/, function() {
  cy.getByTestId("incrementButton").click();
  cy.getByTestId("incrementButton").click();
});

Then(/^I see that input value is "4"$/, function() {
  cy.getByTestId("input").should("have.attr", "value", "4");
});
