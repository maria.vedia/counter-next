import { Given, Then } from "cypress-cucumber-preprocessor/steps";
const url = "http://localhost:3000/";

Given(/^I open my Counter App$/, function() {
  cy.visit(url);
});

Then(
  /^I see "¿En cuántas oficinas quieres usar este producto" in the title$/,
  function() {
    cy.getByTestId("counterTitle").contains(
      "¿En cuántas oficinas quieres usar este producto?"
    );
  }
);
