module.exports = {
  setupFilesAfterEnv: ["<rootDir>/jest.setup.js"],
  testPathIgnorePatterns: [
    "<rootDir>/.next/', '<rootDir>/node_modules/",
    "<rootDir>/cypress"
  ],
  verbose: true,
  moduleDirectories: ["node_modules", "src"],
  moduleNameMapper: {
    "\\.(scss)$": "<rootDir>/styleMock.js"
  }
};
