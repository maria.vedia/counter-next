import * as jest from "jest";
import "jest-dom/extend-expect";
import "react-testing-library/cleanup-after-each";
jest.mock("next/link", () => {
  return ({ children }) => {
    return children;
  };
});
