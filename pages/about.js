import Link from "next/link";

const About = () => (
  <>
    <header>
      <Link href="/">
        <a>Counter</a>
      </Link>
    </header>
    <section>
      <h1>About</h1>
      <p>Somos una multinacional bla bla bla</p>
    </section>
  </>
);
export default About;
