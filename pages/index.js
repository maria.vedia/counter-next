import Link from "next/link";
import Counter from "../components/Counter/Counter";
import styles from "../scss/main.scss";

const Home = () => (
  <>
    <header>
      <Link href="/">
        <a>Counter</a>
      </Link>
      <Link href="/about">
        <a>About</a>
      </Link>
    </header>
    <Counter />
  </>
);
export default Home;
